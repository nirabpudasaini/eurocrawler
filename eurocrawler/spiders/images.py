from scrapy.spiders import CrawlSpider, Rule

class linksSpider(CrawlSpider):
    name = 'images'
    allowed_domains = ['europa.eu']
    start_urls = ['https://europa.eu/european-union/index_en']

    def parse(self,response):
        for images in response.css("img"):
            yield{
                'link' : images.xpath('@src').get(),
                'alt' : images.xpath('@alt').get(),
            }
        
        for a in response.css("a"):
            yield response.follow(a,callback=self.parse)