from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.item import Item, Field

class MyItem(Item):
    url= Field()


class linksSpider(CrawlSpider):
    name = 'links'
    allowed_domains = ['europa.eu']
    start_urls = ['https://europa.eu/european-union/index_en']
    """rules = (Rule(LxmlLinkExtractor(allow=()), callback='parse_obj', follow=True),)"""

    def parse(self,response):
        for images in response.css("img"):
            yield{
                'link' : images.xpath('@src').get(),
                'alt' : images.xpath('@alt').get(),
            }
        
        for a in response.css("a"):
            yield response.follow(a,callback=self.parse)
"""         item = MyItem()
        item['url'] = []
        for link in LxmlLinkExtractor(allow=()).extract_links(response):
            item['url'].append(link.url)
        return item """